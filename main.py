#
#
# 
#
# 
# 
#
#
#     
#
# 
# 
# 
# 
# 
#
import os
import cgi
import datetime
import urllib
import webapp2
import jinja2
import re
import hashlib

from models import *
from datetime import *
from string import letters
from google.appengine.ext import db
from google.appengine.ext import ndb
from google.appengine.api import users


#---------------------------< JINJA2 ENVIRONMENT >------------------------------------------
template_dir = os.path.join(os.path.dirname(__file__),'models')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

#------------------------------< GLOBAL METHODS >--------------------------------------
def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

def genHash(nombre, apellido):
    edge = nombre[0] + apellido[0]
    value = nombre + apellido
    result = int(hashlib.sha1(value).hexdigest(),16) % (10 ** 4)
    result = edge + str(result)
    
    return result
#-------------------------< MAIN CLASSES >---------------------------------------------

############<<<<MAIN HANDLER FOR RENDERING CALLED--[ Renderer ]-- >>>#########
# We will inherit this class to the others to render the html

class Renderer(webapp2.RequestHandler):
    def write(self, *a, **params):
        self.response.out.write(*a, **params)
    
    def render_str(self, template, **params):
        return render_str(template, **params)
    
    def render(self, template, **params):
        self.write(self.render_str(template, **params))
    
    @webapp2.cached_property
    def jinja2(self):
        # Returns a Jinja2 renderer cached in the app registry.
        return jinja2.get_jinja2(factory=jinja2_factory)
    
############################<<<<<<<< Controlers >>>>>>>>>############################

class Candidatos(Renderer):
    def get(self, ):
        #candidato = Candidato.get_by_id(user_id, Candidato )   
        #self.write(user_id)
        self.render("candidato.html")
        
    def post(self, ):
        
        candidato = Candidato()
        
        candidato.nombre = self.request.get('nombre')
        candidato.apellidos = self.request.get('apellidos')
        candidato.origen = self.request.get('origen')
        candidato.telefono = self.request.get('telefono')
        candidato.correo = self.request.get('correo')
        candidato.direccion = self.request.get('direccion')
        candidato.uniqueID = genHash(candidato.nombre,candidato.apellidos)
        
        candidatoLocation = CandidatoLocation()
        candidatoLocation.location = ndb.GeoPt(self.request.get('latlon'))
        
        candidato.localizacionActual.append(candidatoLocation) 
        #candidato.valoracion = self.request.get('')
        
        clave = candidato.uniqueID
        
        candidato.put()
        self.redirect('/portal/'+candidato.uniqueID)
        #self.write('<h1>Gracias por suscribirte</h1><br><h3>Tu clave es:</h3><p>  '+candidato.uniqueID + '</p>')
 
class Portal(Renderer):        
        def get(self, user_id ):
            user_id = user_id.upper()
            candidato = Candidato.query(Candidato.uniqueID == user_id).get()
            ongs = Ong.query()
            proyectos = Proyecto.query()
            if candidato:
                self.render('portal.html', candidato = candidato,proyectos = proyectos,ongs = ongs)
            else:
                self.redirect('/')
        
class Empleadores(Renderer):
    def get(self, empleador_id ):
        empleador = Empleador.query(Empleador.rfc == empleador_id).get()
        proyectos = Proyecto.query(Proyecto.empleadorRfc == empleador_id)
        
        user = users.get_current_user()

        if not user:
            login = True
            link = users.create_login_url('/')
            #self.render('index.html', link = link, login = login)
        else:
            login = False
            link = users.create_logout_url('/')
            #user = user.nickname()
            existe = False
            empleadores = Empleador.query()
            
            for e in empleadores:
                if user.email() == e.correo:
                    existe = True
            
            
        self.render('portalEmpleadores.html', empleador = empleador, proyectos = proyectos, login = login, link = link)
    
        
        #self.write(empleador)
    
    
    def post(self, ):
        empleador = Empleador()
        
        empleador.nombreEmpresa = self.request.get('nombreEmpresa')
        empleador.rfc = self.request.get('rfc')
        empleador.nombre = self.request.get('nombre')
        empleador.apellidos = self.request.get('apellidos')
        empleador.tel = self.request.get('tel')
        empleador.correo = self.request.get('correo')
        empleador.direccion = self.request.get('direccion')
        empleador.valoracion= 0.0
        empleador.put()
        self.redirect('/empleador/'+empleador.rfc)
        
        
class Ongs(Renderer):
    def get(self, ):
        self.render('Pagina de Ongs')
    
    def post(self, ):
        ong = Ong()
        ong.nombre = self.request.get('nombre')
        ong.correo = self.request.get('correo')
        ong.telefono = self.request.get('tel')
        ong.cede = self.request.get('cede')
        ong.direccion = self.request.get('direccion')
        ong.webUrl = self.request.get('url')
        ong.put()
        
        self.redirect('/')
        
class Proyectos(Renderer):
    def get(self, ):
        self.render('Pagina de Proyectos')
    
    def post(self, ):
        proyecto = Proyecto()
        
        proyecto.titulo = self.request.get('titulo')
        proyecto.direccionATrabajar = self.request.get('direccionATrabajar')
        proyecto.pais = self.request.get('pais')
        proyecto.estado = self.request.get('estado')
        proyecto.ciudad = self.request.get('ciudad')
        proyecto.empleador = self.request.get('empleador')
        proyecto.empleadorRfc = self.request.get('rfc')
        proyecto.beneficios = self.request.get('beneficios')
        proyecto.fechaInicio = datetime.strptime(self.request.get('fechaInicio'),"%Y-%m-%d")
        proyecto.fechaTermino = datetime.strptime(self.request.get('fechaTermino'),"%Y-%m-%d")
        proyecto.descripcion = self.request.get('descripcion')
        if self.request.get('limiteEmpleados'):
            proyecto.limiteEmpleados = int(self.request.get('limiteEmpleados'))
        #proyecto.candidatos = [self.request.get('candidatos')]
        proyecto.geoLocation = ndb.GeoPt(self.request.get('latlon'))
        proyecto.put()
        if proyecto.titulo and proyecto.empleador and proyecto.empleadorRfc:
            self.redirect('/empleador/'+proyecto.empleadorRfc)
        else:
            self.redirect('/')
class Inscripcion(Renderer):
    def get(self, ):
        pro = int(self.request.GET['proyecto'])
        can = str(self.request.GET['candidato'])
        proyecto = ndb.Key(Proyecto, pro).get()
        if str(can) not in proyecto.candidatos:
            
            proyecto.candidatos.append(str(can))
            self.redirect('/portal/'+can)
        else:
            self.redirect('/portal/'+can)
        #self.write(str(proyecto)+"<br>   " + str(pro)+" ------  "+ str(can) )

        proyecto.put()
        #self.write(str(proyecto) +"   " + str(pro)+"  "+ str(can) )
    
    def post(self, ):
        x = self.post.get('parametro')
        self.write(x)
    

class ProyectForm(Renderer):
    def get(self, empleador_id ):
        #self.render('proyectForm.html')
        empleador = ndb.Key(Empleador, empleador_id).get()
        empleador = Empleador.query(Empleador.rfc == empleador_id).get()
        
        
        self.render('dataLists.html', empleador = empleador, proyectos = proyectos)

class EmployerForm(Renderer):
    def get(self, ):
        self.render('empleadorForm.html')
    
class OngForm(Renderer):
    def get(self, ):
        self.render('ongform.html')
    


class MainHandler(Renderer):
    def get(self):
        
        user = users.get_current_user()
        if not user:
            login = True
            link = users.create_login_url('/')
            self.render('index.html', link = link, login = login)
        else:
            email = user.email()
            #self.write(email)
            e = Empleador.query(Empleador.correo == email).get()
            #self.write(e)

            if e :
                self.redirect('/empleador/'+e.rfc)
            
            
            else:
                login = False
                link = users.create_logout_url('/')
                #user = user.nickname()
                existe = False
                empleadores = Empleador.query()
                self.redirect(users.create_logout_url('/'))                #for empleador in empleadores:
                 #   if user.email() == empleador.correo:
                  #      existe = True
                
                self.write('hola')
                #self.redirect('/')
                #self.render('index.html', user = user, login = login)
                

####################<< TESTING AREA >>>#############################################
####################################################################################

class Tester(Renderer):
    def get(self,):
        #user = Candidato.query(Candidato.uniqueID == parameter).get()
        
        #self.write(str(user) + parameter)
        self.render('main.html', mensaje = "hola")



###################################################################################

app = webapp2.WSGIApplication([
    ('/portal/(.*)', Portal),
    ('/', MainHandler),
    ('/candidato',Candidatos),
    ('/empleador',Empleadores),
    ('/empleador/(.*)',Empleadores),
    ('/formaProyecto/(.*)',ProyectForm),
    ('/formaEmpleador',EmployerForm),
    ('/formaOng',OngForm),
    ('/formaCandidato',),
    ('/ong',Ongs),
    ('/proyecto',Proyectos),
    ('/testing', Tester),
    ('/inscripcion', Inscripcion)
    
], debug=True)
