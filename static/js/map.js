var map;
var localizacion;
var geocoder;
var infowindow = new google.maps.InfoWindow();
var marker;
var actuallLocation;
var latlon;
var objects = [];
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();


function addLocation( ) {
    list = document.getElementById('proyectos').value
    total = list.split( ':' )
    //alert(total)
    
    for (i =0 ; i < total.length -1 ; i++ ) {
        valor = total[i];
        valor = valor.replace('[','')
        valor = valor.replace(']','')
        lugar = valor.split(',')
        nombre = String(lugar[0]);
        //alert(nombre);
        location_lan = parseFloat(lugar[1]);
        //alert(location_lan);
        location_lon = parseFloat(lugar[2]);
        //alert(location_lon);
        
    objects.push([nombre,location_lan,location_lon])
    //alert([String(nombre),location_lan,location_lon])
    } 
}

function initialize( objeto ) {
    directionsDisplay = new google.maps.DirectionsRenderer();
    geocoder = new google.maps.Geocoder();
    var mapOptions = {
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.ROADMAP 
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
    
    getLocation('latlon')
    directionsDisplay.setMap(map);
    directionsDisplay.setP
    //addLocation()
    //alert(objects) 
    //setMarkers(map,objects);
    
  
}

function calcRoute(lat, lon ) {
    start = actuallLocation;
    end = new google.maps.LatLng(lat,lon)
    var request={
        origin: start,
        destination: end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
        
    };
    
    directionsService.route(request, function(response, status){
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
        
    });
}

// Metodo para los marcadores en el mapa

function setMarker(map,lat,lon,color) {
    //alert(location)
    //var beach = location.split(',');
    var myLatLng = new google.maps.LatLng(lat, lon);
    var marker = new google.maps.Marker({ 
        icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
        position: myLatLng,
        map: map,
        zIndex:1
        //icon: image,
        //shape: shape,
        //title: beach[0],
        //zIndex: beach[3]
    });
    map.setCenter(marker.position);
}

function setMarkers(map, locations) {
  // Add markers to the map

  // Marker sizes are expressed as a Size of X,Y
  // where the origin of the image (0,0) is located
  // in the top left of the image.

  // Origins, anchor positions and coordinates of the marker
  // increase in the X direction to the right and in
  // the Y direction down.
  /*
  var image = {
    url: 'images/beachflag.png',
    // This marker is 20 pixels wide by 32 pixels tall.
    size: new google.maps.Size(20, 32),
    // The origin for this image is 0,0.
    origin: new google.maps.Point(0,0),
    // The anchor for this image is the base of the flagpole at 0,32.
    anchor: new google.maps.Point(0, 32)
  };*/
  // Shapes define the clickable region of the icon.
  // The type defines an HTML &lt;area&gt; element 'poly' which
  // traces out a polygon as a series of X,Y points. The final
  // coordinate closes the poly by connecting to the first
  // coordinate.
  var shape = {
      coord: [1, 1, 1, 20, 18, 20, 18 , 1],
      type: 'poly'
  };
  for (var i = 0; i < locations.length; i++) {
    var beach = locations[i];
    var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
    var marker = new google.maps.Marker({
        icon:'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
        position: myLatLng,
        map: map,
        //icon: image,
        //shape: shape,
        title: beach[0],
        //zIndex: beach[3]
    });
  }
}


// Fin del metodo de marcadores

function getLocation(objeto) {
    if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
        pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);
        /*
        var infowindow = new google.maps.InfoWindow({
            map: map,
            position: pos,
            content: 'Localizacion Actual'
            
        });
        */
        //x = document.getElementById(objeto).value = pos;
        map.setCenter(pos)
        actuallLocation = pos;
        //localizacion = document.getElementById(objeto).value = pos;
        pos = new String(pos)
        pos = pos.slice(1,pos.length-1)
        codeLatLng(pos,objeto)
    }, function() {
      handleNoGeolocation(true);
    });
    
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }
}

function codeLatLng(input,objeto) {
  //var input = document.getElementById('latlng').value;
  var latlngStr = input.split(',', 2);
  var lat = parseFloat(latlngStr[0]);
  var lng = parseFloat(latlngStr[1]);
  var latlng = new google.maps.LatLng(lat, lng);
  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        map.setZoom(17);
        marker = new google.maps.Marker({
            position: latlng,
            map: map
        });
        infowindow.setContent(results[1].formatted_address);
        infowindow.open(map, marker);
        if (document.getElementById(objeto) != null) {
                        document.getElementById(objeto).value = results[1].formatted_address;
                    document.getElementById('latlon').value = pos;
                    document.getElementById('position').value = results[1].formatted_address;
        }
      } else {
        alert('No results found');
      }
    } else {
      alert('Geocoder failed due to: ' + status);
    }
  });
}



function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
  
  
}
google.maps.event.addDomListener(window, 'load', initialize);


