
from google.appengine.ext import ndb

###########################<<< DATA BASE >>>#####################################
###########################<<< DATA BASE >>>#####################################
################<<<< ESTRUCTURA DE LOS OBJETOS DE CADA TABLA>>>>#################

class CandidatoLocation(ndb.Model):
        location = ndb.GeoPtProperty()
        fecha = ndb.DateProperty( auto_now_add = True)
    


class Candidato(ndb.Model):
    #contiene 11 variables  
# Estos datos se obitienen automaticamente
    
    localizacionActual = ndb.StructuredProperty(CandidatoLocation, repeated = True)
    registro = ndb.DateProperty(auto_now_add = True)
    uniqueID = ndb.StringProperty()
# Ingresados por el usuario
    nombre = ndb.StringProperty()
    apellidos = ndb.StringProperty()
    direccion  = ndb.StringProperty()
    telefono = ndb.StringProperty()
    origen = ndb.StringProperty()
    correo = ndb.StringProperty()
    
    
class Empleador(ndb.Model):    
# Automatic fetch
    registro = ndb.DateProperty(auto_now_add = True) 
# User submition    
    nombreEmpresa = ndb.StringProperty()
    rfc = ndb.StringProperty() # must be checket
    nombre  = ndb.StringProperty()
    apellidos = ndb.StringProperty()
    tel = ndb.StringProperty()
    correo = ndb.StringProperty()
    direccion = ndb.StringProperty()
    valoracion = ndb.FloatProperty()
    
class Ong(ndb.Model):
# Automatic fetch
    uniqueID = ndb.StringProperty()
    registo = ndb.DateProperty(auto_now_add = True)
# user submitted, But will be checked by
# an administrator and then allowed to be registered
    
    nombre = ndb.StringProperty()
    correo = ndb.StringProperty()
    telefono = ndb.StringProperty()
    cede = ndb.StringProperty()
    direccion = ndb.StringProperty()
    webUrl = ndb.StringProperty()

class Noticia(ndb.Model):
    titulo = ndb.StringProperty()
    autor = ndb.StringProperty()
    fecha = ndb.DateTimeProperty(auto_now_add = True)
    contenido = ndb.TextProperty()

class Proyecto(ndb.Model):
# Automatic fetch
    registro = ndb.DateProperty( auto_now_add = True)
    candidatos = ndb.StringProperty( repeated = True)
    geoLocation = ndb.GeoPtProperty()
    
# user submitted data    
    titulo = ndb.StringProperty()
    direccionATrabajar = ndb.StringProperty()
    pais = ndb.StringProperty()
    estado = ndb.StringProperty()
    ciudad = ndb.StringProperty()
    empleador = ndb.StringProperty()
    empleadorRfc = ndb.StringProperty()
    fechaInicio = ndb.DateProperty()
    fechaTermino = ndb.DateProperty()
    descripcion = ndb.TextProperty()
    limiteEmpleados = ndb.IntegerProperty()
    beneficios = ndb.TextProperty()
    
    
class Itinerario(ndb.Model):
    candidato = ndb.StringProperty()
 
    
######################<<< END OF DATABASE STRUCTIRATION >>>>#####################